import React from "react";
import { HeroBanner, Product, FooterBanner } from "../components";
import { client } from "../lib/client";
const index = ({ banners, products }) => {
  return (
    <div>
      <HeroBanner heroBanner={banners.length > 0 && banners[1]} />
      <div className="products-heading ">
        <h2 className="italic_font">Best Selling Products</h2>
        <p>Speakers of many variations</p>
      </div>
      <div className="products-container">
        {products?.map((product) => (
          <Product key={product._id} product={product} />
        ))}
      </div>
      <FooterBanner footerBanner={banners.length > 0 && banners[0]} />
    </div>
  );
};

export const getServerSideProps = async () => {
  const query = '*[_type == "product"]';
  const products = await client.fetch(query);

  const bannerQuery = '*[_type== "banner"]';
  const banners = await client.fetch(bannerQuery);
  return {
    props: {
      products,
      banners,
    },
  };
};

export default index;
